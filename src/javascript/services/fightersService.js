import { callApi } from '../helpers/apiHelper';
import { fightersDetails } from '../helpers/mockData';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    const endpoint = `details/fighter/${id}.json`;
    let fightersDetails;
    await callApi(endpoint, 'GET')
      .then(response => fightersDetails = response)
    return fightersDetails;
  }
}

export const fighterService = new FighterService();
