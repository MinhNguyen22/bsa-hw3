import {showModal} from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const fighterImage = createFighterImage(fighter);

  const showModalArgument = {
    title: fighter.name,
    bodyElement: fighterImage
  };
  showModal(showModalArgument);
}
