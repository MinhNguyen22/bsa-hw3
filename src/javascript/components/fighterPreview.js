import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter !== undefined) {
    fighterElement.appendChild(createFighterImage(fighter));

    const description = createElement({
      tagName: 'div'
    });

    fighterElement.appendChild(description);
    for (const [statName, statValue] of Object.entries(fighter)) {
      if (statName != 'source' && statName != '_id')
        description.appendChild(createStatDescription(statName, statValue));
    }
  }

  return fighterElement;
}

const createStatDescription = (statName, statValue) => {
  const descriptionElement = createElement({
    tagName: 'p'
  });
  descriptionElement.innerHTML = `${statName}: ${statValue};`;
  descriptionElement.style = 'color: green; font-weight: bolder; text-shadow: black 2px 2px 2px;  text-transform: capitalize; margin: 0 0';
  return descriptionElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
